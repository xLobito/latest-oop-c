#ifndef PRUEBA_H
#define PRUEBA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define private(T) struct T ## Private *T##Private;
 
typedef struct PruebaPrivate PruebaPrivate;
typedef struct PruebaDerivadaPrivate PruebaDerivadaPrivate;

typedef struct Prueba
{
    int x;
    int y;
    struct PruebaPrivate *private;
    int (*GetPrivate)(struct Prueba *);
} Prueba;

typedef struct PruebaDerivada
{
    Prueba;
    int w;
    private(PruebaDerivada);
    int (*GetPrivateDerivada)(struct PruebaDerivada *);
} PruebaDerivada;

extern void *Prueba_ctor(int, int, int);
extern void *PruebaDerivada_ctor(int, int, int, int);
extern int Prueba_GetPrivate(Prueba *);
extern int PruebaDerivada_GetPrivateDerivada(struct PruebaDerivada *);

#endif /* PRUEBA_H */