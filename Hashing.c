#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct 
{
    char Email[100];	
	char Nombre[30];
	char Apellido[30];
	char Compania[30];
	char Direccion[40];
	char Ciudad[30];
	char Pais[30];
	char Provincia[30];
	char CodigoZip[20];
	char Telefono1[20];
	char Telefono2[20];
} Persona;

typedef struct Lista
{
    Persona *p;
    struct Lista *sig;
} Lista;

FILE *f = NULL;

void IniciarHash(Lista **);
void Insertar(Lista **);
void Hash(Persona *, Lista **);
int EjecutarFuncionHash(const char *);
void AgregarElemento(Persona *, Lista **L);
void Recorrer(Persona *p, Lista *L);
void Modificar(const char *, Lista **);

int main(void)
{
    Lista *indice[100];
    int opcion = 1;
    
    IniciarHash(indice);
    do
    {
        printf("1. Insertar\n");
        printf("2. Modificar\n");
        printf("0. Salir\n");
        scanf("%d", &opcion);
        switch(opcion)
        {
            case 1:
                Insertar(indice);
                break;
            case 2:
                Modificar("cualquierCosa@gmail.com", indice);
                break;
            default:
                break;
        }
    } while(opcion != 0);
    /*for(int i=0; i<500; i++)
    {
        Insertar(indice);
    } */
    return 0;
}

void IniciarHash(Lista **indice)
{
    for(int i=0; i<100; i++)
    {
        indice[i] = NULL;
    }
    return;
}

void Insertar(Lista **indice)
{
    Persona *aux = malloc(sizeof(Persona));

    if(!f)
    {
        f = fopen("us-500.csv", "rb+");
        if(!f)
        {
            abort();
        }
    }
    fseek(f, ftell(f), SEEK_SET);
    fread(aux, sizeof(Persona), 1, f);
    Hash(aux, indice);
    return;
}

void Hash(Persona *p, Lista **indice)
{
    int bloque = EjecutarFuncionHash((*p).Email);
    
    AgregarElemento(p, &indice[bloque]);
    return;
}

int EjecutarFuncionHash(const char *clave)
{
    int asciiSuma;

    for(int i=0; i<strlen(clave); i++)
    {
        asciiSuma += (int)clave[i];
    }
    return (asciiSuma % 100);
}

void AgregarElemento(Persona *p, Lista **L)
{
    Lista *aux = malloc(sizeof(Lista));

    if((*L) == NULL)
    {
        aux->p = p;
        aux->sig = (*L);
        (*L) = aux;
    }
    else 
    {
        free(aux);
        return AgregarElemento(p, &(*L)->sig);
    }
    return;
}

void Modificar(const char *email, Lista **indice)
{
    int fhash;
    Persona *aux = malloc(sizeof(Persona));

    if(!email)
    {
        return;
    }
    else 
    {
        fhash = EjecutarFuncionHash(email);
        if(!strcmp(email, (*indice[fhash]).p->Email))
        {
            printf("Insertar nombre: ");
            fgets(aux->Nombre, 30, stdin);
            fflush(stdin);
            printf("Insertar apellido:  ");
            fgets(aux->Apellido, 30, stdin);
            fflush(stdin);
            printf("Insertar compañía:  ");
            fgets(aux->Compania, 30, stdin);
            fflush(stdin);
            printf("Insertar dirección:  ");
            fgets(aux->Direccion, 40, stdin);
            fflush(stdin);
            printf("Insertar ciudad:  ");
            fgets(aux->Ciudad, 30, stdin);
            fflush(stdin);
            printf("Insertar país:  ");
            fgets(aux->Pais, 30, stdin);
            fflush(stdin);
            printf("Insertar provincia:  ");
            fgets(aux->Provincia, 30, stdin);
            fflush(stdin);
            printf("Insertar código ZIP:  ");
            fgets(aux->CodigoZip, 20, stdin);
            fflush(stdin);
            printf("Insertar número de teléfono:  ");
            fgets(aux->Telefono1, 20, stdin);
            fflush(stdin);
            printf("Insertar número de teléfono alternativo:  ");
            fgets(aux->Telefono2, 20, stdin);
            fflush(stdin);
            strcpy(aux->Email, email);
            puts("\nModificación completa\n");
            memcpy((*indice)[fhash].p, aux, sizeof(*aux)); 
        }
        else
        {
            printf("No existe el registro en cuestión\n");
            return;
        }
    }
    return;
}