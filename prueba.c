#include "prueba.h"

struct PruebaPrivate
{
    int z;
};

struct PruebaDerivadaPrivate
{
    int w;
};

__attribute__((optimize("O0"))) void *Prueba_ctor(int x, int y, int z)
{
    Prueba *this = malloc(sizeof(Prueba));

    this->x = x;
    this->y = y;
    this->private = malloc(sizeof(struct PruebaPrivate));
    this->private->z = z;
    this->GetPrivate = &Prueba_GetPrivate;
    return this;
}

__attribute__((optimize("O0"))) void *PruebaDerivada_ctor(int x, int y, int z, int w)
{
    PruebaDerivada *this = malloc(sizeof(PruebaDerivada));

    memcpy(this, Prueba_ctor(x, y, z), sizeof(struct Prueba));
    this->PruebaDerivadaPrivate = malloc(sizeof(struct PruebaDerivadaPrivate));
    this->PruebaDerivadaPrivate->w = w;
    this->GetPrivateDerivada = &PruebaDerivada_GetPrivateDerivada;
}

int Prueba_GetPrivate(Prueba *this)
{
    return this->private->z;
}

int PruebaDerivada_GetPrivateDerivada(PruebaDerivada *this)
{
    return this->PruebaDerivadaPrivate->w;
}