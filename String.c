#include "String.h"

typedef struct StringPrivate
{
	Object object;
    char *string;
	void (*SetString)(const char *);
	void (*SetStringLength)(const char *, size_t);
} StringPrivate;

/* Public */
int String_CompareTo(const char *, String * const);
String *String_Concatenate(const char *, String * const);
char *String_Get(String * const);
int String_Length(String * const);
String *String_ToString(String * const);

/* Ctor */
void *String_String0(String *);
void *String_String1(const char *, String *);
void *String_String2(const char *, size_t, String *);

/* Private */
static void StringPrivate_SetString(const char *, StringPrivate * const);
static void StringPrivate_SetStringLength(const char *, size_t, StringPrivate * const);


int String_CompareTo(const char *str, String * const this)
{
	char *str2 = this->StringPrivate->string;
	
	while(*str && (*str == *str2))
		str++, str2++;
	return *(const unsigned char *)str - *(const unsigned char *)str2;
}

String *String_Concatenate(const char *str, String * const this)
{
	String *strAux;
	
	if(str == NULL)
	{
		throw new NullArgumentException();
	}
	strAux = this;
	strAux->StringPrivate->string = GC_REALLOC(strAux->StringPrivate->string, strlen(strAux->StringPrivate->string) + strlen(str) + 1);
	strcat(this->StringPrivate->string, str);
	return this;
}

char *String_Get(String * const this)
{
    return this->StringPrivate->string;
}

int String_Length(String * const this)
{
	return strlen(this->StringPrivate->string);
}

String *String_ToString(String * const this)
{
	return this;
}

/* Ctor */
void *String_String0(String *this)
{
	/* Encapsulate */
	void *__this;
	void *__save_fp = (void *)this->String0;
	callprotect(&this->object);
	__this = Object_create(sizeof(*this), 5);
	memcpy(this, __this, sizeof(*this));
	FUNCTION(String, CompareTo, 1);
	FUNCTION(String, Concatenate, 1);
	FUNCTION(String, Length, 0);
	FUNCTION(String, Get, 0);
	FUNCTION(String, ToString, 0);
	FUNCTION(String, String1, 1);
	FUNCTION(String, String2, 2);
	this->String0 = __save_fp;
	this->StringPrivate = Object_create(sizeof(StringPrivate), 2);
	FUNCTIONP(String, SetString, 1);
	FUNCTIONP(String, SetStringLength, 2);
	Object_prepare(&this->StringPrivate->object);
	//this->StringPrivate->string = GC_MALLOC(255);
	this->StringPrivate->SetString("");
	Object_prepare(&this->object);
	return 0;
}

void *String_String1(const char *str, String *this)
{
	/* Encapsulate */
	void *__this;
	void *__save_fp = (void *)this->String1;
	callprotect(&this->object);
	__this = Object_create(sizeof(*this), 5);
	memcpy(this, __this, sizeof(*this));
	FUNCTION(String, CompareTo, 1);
	FUNCTION(String, Concatenate, 1);
	FUNCTION(String, Length, 0);
	FUNCTION(String, Get, 0);
	FUNCTION(String, ToString, 0);
	FUNCTION(String, String0, 1);
	FUNCTION(String, String2, 2);
	this->String1 = __save_fp;
	this->StringPrivate = Object_create(sizeof(StringPrivate), 2);
	FUNCTIONP(String, SetString, 1);
	Object_prepare(&this->StringPrivate->object);
	this->StringPrivate->SetString(str);
	Object_prepare(&this->object);
	return 0;
}

void *String_String2(const char *str, size_t size, String *this)
{
	void *__this;
	void *__save_fp = (void *)this->String2;
	callprotect(&this->object);
	__this = Object_create(sizeof(*this), 5);
	memcpy(this, __this, sizeof(*this));
	FUNCTION(String, CompareTo, 1);
	FUNCTION(String, Concatenate, 1);
	FUNCTION(String, Length, 0);
	FUNCTION(String, Get, 0);
	FUNCTION(String, ToString, 0);
	FUNCTION(String, String0, 1);
	FUNCTION(String, String1, 2);
	this->String2 = __save_fp;
	this->StringPrivate = Object_create(sizeof(StringPrivate), 2);
	FUNCTIONP(String, SetString, 1);
	FUNCTIONP(String, SetStringLength, 2);
	Object_prepare(&this->StringPrivate->object);
	this->StringPrivate->SetStringLength(str, size);
	Object_prepare(&this->object);
	return 0;
}

/* Private */
static void StringPrivate_SetString(const char *str, StringPrivate * const this)
{
	this->string = GC_MALLOC(255);
	strcpy(this->string, str);
	this->string[strlen(str)] = '\0';
	return;
}

static void StringPrivate_SetStringLength(const char *str, size_t size, StringPrivate * const this)
{
	this->string = GC_MALLOC(255);
	strcpy(this->string, str);
	this->string[strlen(str)] = '\0';
	return;
}