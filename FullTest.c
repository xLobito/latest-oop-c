/* Include header */
#include "ObjectPrueba.h"

/**
 * Basic definition for any class-type storage in either Standard C or 
 * GNU C (*nix based). 
 * The definition shall contain specific attributes to work as expect by manually specify 
 * count and flag for the Virtual Table. 
 * Same as before, if the programmer uses the Virtual Table then it's mandatory targeting 
 * the vtable under the hood by calling "vtable(T)" which T refers to the type of the class-type storage.
 * 
 * The constructor and the methods must be defined as "Pointer to function" C-style syntax 'cause in C declaration and 
 * definition is not available. This should be rewritten by a new compiler. [WIP]¨.
 * 
 * Note: vtable(T) is considered as type-safe if T respects the class-type storage of the Virtual Table target.
 * Note: The constructor itself needs the class-type storage name as well as the number of the argument attached to it. 
 * This occurs 'cause the "overload" is the "global scope" of the constructor which internally refers to the constructor method
 * provided by joining the name and the number of arguments passed to it. 
 */
typedef struct A
{
    /* Mandatory */
    Object object;
    /* Just for now, this is expected to be explicitly defined. Later it will be encapsulated as an anonymous structure (Hidden for programmer) */
    struct
    {
        /* Refers the count of Virtual Methods which belong to a class */
        int __vptrCount;
        /* If the Virtual Table has not been initialized. This is set to false, or true otherwise. */
        int __vptrFlag;
    };
    /**
     * By using "PIMPL" idiom (Or Pointer to Implementation) which was an old C++ feature, points to a Virtual Table 
     * statically defined elsewhere (In a shared object). 
     * The Virtual Table will have an arbitrary allocated-space for keeping the virtual function-like memory address 
     * marked; to be called at runtime dynamically depending on the class (Of course, in an hierarchical environment). 
     */
    vtable(A);
    /**
     * Description:
     * 
     * The function overloading (known as method overloading either) is useful for calling a method with different parameters 
     * using the same name. 
     * For this specific case-scenario, is restricted exclusively to a constructor which can be called with multiple paramaters
     * (zero is included as well).
     * 
     * Proposal:
     * 
     * As slightly defined before, the implementation is defining a macro with a variadic number of arguments (__VA_ARGS__) which will 
     * expand to overload macro by taking the type and the variadic arguments. The macro will make the underlying arrangements by
     * placing the methods to be jumped out. 
     * 
     * Note: As a matter of fact, the macro itself ain't the overloading. The constructor as a method needs to be defined in conjunction with the
     * macro. The syntax for the constructor (overloaded) must have the following signature.
     * 
     * type (*Tx)(...)
     * 
     * Where:
     * 
     * type -> Expected method return type.
     * T -> Class type name. 
     * x -> Number of arguments expected by the constructor.
     * ... -> The expected arguments type (Must have the same parameters as x in the constructor name). 
     */
    #define A(args...) overload(A, args);
    void *(*A0)(void);
    /**
     * The class destructor is necessary to "clean up" any handle or memory allocated by the class itself. 
     * When the pointer is heap-based the Garbage Collector registers this method and calls it whenever necessary or when the
     * class goes out of scope (In abnormal cases, the calling occurs when an exception is thrown and Stack Unwinding is performed). 
     */
    void (*$A)(void);
    /**
     * Standard way for method declaration (Definition is expected to be downside). 
     * Every method (even virtual ones) should be declared using this syntax.
     * 
     * Note: Virtual methods are specified in the constructor by calling the "virtual" macro which statically marks the pointer
     * to a Virtual Table and the rellocates the behavior by selecting the hierarchical class who calls it. 
     */
    void (*virtualFunc)(int);
    void (*virtualFunc2)(void);
} A;

/**
 * When a class is inherited, the "superclass" (known as base class) add its elements (Attributes and Methods) to the 
 * subclass (or derived class), so, these kind of classes are literally joined. 
 * The procedure to manipulate these classes are practically the same as before with one big difference which is the class
 * expected-to-be-inherited.  
 * In certain programming languages (Like C++ or C#) the syntax is expected to be sort of like this:
 * 
 * C++:
 * class derived : public base
 * {}
 * 
 * C#:
 * class derived : base
 * {}
 * 
 * In my own proposal, using the latest C11 (ISO 9899:2011) and the anonymous structures, provide some composition between 
 * the super and subclass to work as in a hierarchical and Object Oriented programming language. The true composition is well-defined in here,
 * yet, the Dreadful Diamond on Derivation is not solved 'cause Virtual Inheritance ain't possible without a compiler which checks out for a 
 * multiple redefinition. So, its use is forbidden.
 */
typedef struct B
{
    /* Composition with the base class */
    A;
    /* Rest of it. Just like A class-like */
    vtable(B);
    #define B(args...) overload(B, args);
    void *(*B0)(void);
    void (*$B)(void);
} B;

/**
 * The method declaration in the class-like are "Pointer to Functions", therefore it can't be 
 * considered as functions; just points to a memory address which belongs to a function. 
 * So, the definition of functions are provided outside the class-like definition by using old C++ technique, 
 * using the class-like name and the method name of it. 
 * 
 * The inner workings is by using the memory-mapping (mmap function) and a "trampoline" which overwrites the method 
 * at the stack (Statically defined) and saves some space for the class-like pointer to be jumped out to it keeping the self pointer (Known as this)
 * with runtime information.
 * The implementation is OS-specific (x64 - *nix based) with GNU Assembly and some trickery C shellcode. 
 * 
 * Note: The "this" pointer ALWAYS has to be defined as a constant pointer. This is the declaration in early versions of C++ and passed to later generations.
 */
void *A_A0(A * const);
void A_virtualFunc(int, A * const);
void A_virtualFunc2(A * const);
void A_$A(A * const);
void *B_B0(B * const);
void B_virtualFunc(int, B * const);
void B_virtualFunc2(B * const);
void B_$B(B * const);
void FunctionThrow(void);

/* Main */
optimize(0) int main(void)
{
    /**
     * Instance macro is used when a pointer to a class is created. 
     * This is the same idea as "operator overloading" in C++ but keeping the 
     * structured syntax from C. 
     * The basic idea of Instance is provide the memory address of the new object to be created and 
     * allocate space for the constructor to be called immediately after. 
     * 
     * Instance takes two arguments. The class name and the number of arguments expected for the constructor.
     * Take in count the fact if the number of arguments doesn't match with the constructor to be called then 
     * undefined behavior is expected to come. 
     */
    A *a = new Instance(A, 0);
    B *b = new Instance(B, 0);
    A *f;
    Array(String) *c;
    Int32 *d[5];
    List(String) *e;

    /* Exception handling structure */
    try
    {
        Console->WriteLine("1. Constructors and Polymorphism");
        Console->WriteLine("");
        /* Constructor call */
        a->A();
        /**
         * Whenever there is some virtual method, the rightful way for calling it 
         * is by taking the index on the Virtual Table and the call the vptr() with the 
         * expected parameters. This is the true face of the runtime polymorphism (It will vary between A and B classes for this case only).
         * The system will provide changes to a method whenever an override appears in the subclass constructor or will keep the base Virtual Table 
         * on its inherited classes.  
         */
        /* Virtual call */
        Console->WriteLine("Virtual method call");
        a->vmtA[0].vptr(1);
        a->vmtA[1].vptr();
        /* Constructor call */
        b->B();
        /* Virtual overriden call */
        Console->WriteLine("Method overriden call");
        b->vmtB[0].vptr(1);
        b->vmtB[1].vptr();
        Console->WriteLine("");
        Console->WriteLine("2. Runtime Polymorphism");
        /**
         * When you cast the inherited class into the base class, the attributes belonging to 
         * the inherited class will go away. That's 'cause the cast is shinkring the inherited class
         * to fit into the base class. 
         * However, at runtime polymorphism. As its name dictates, the virtual methods marked in the 
         * constructor will remain. By meaning, when the base class who took the attributes and methods of the
         * inherited class calls the virtual method, then it will perform as the inherited virtual method
         * keeping its changes at execution time. 
         */
        b->virtualFunc(20);
        f = (A *)b;
        f->vmtA[0].vptr(30);
        f->virtualFunc(40);
        Console->WriteLine("");
        Console->WriteLine("3. Exception Handling (Segmentation Fault)");
        /* Set a null pointer */
        b = NULL;
        /* Segmentation Fault. Virtual Table has memory address but class is NULL defined. Points to nowhere */
        b->vmtB[0].vptr(20);
    }
    /**
     * When an exception is raised, the catch block will try to restrain the exception (Only if Exception or the specific Exception type exists)
     * and performs handling to resume normal execution. In the above case, the MemoryAccessException catch clause will restraint the exception and 
     * it will show some useful information (Even the Stack Trace of the function called backwards starting from main).
     */
    catch(MemoryAccessException e)
    {
        Console->WriteLine("In MemoryAccessException block");
        Console->WriteLine("Exception caught: %s", e->ExceptionMessage);
        Console->WriteLine("Exception type: %s", e->ExceptionType);
        e->StackTrace();
    }
    /**
     * In the other hand, if the Exception is defined (This occurs in case of no specific exception is expected to be thrown), depending on the Exception 
     * the information shall vary between the different types or will show a simple message for "generic" Exception type with no further explanations
     * about the error itself. 
     */
    catch(Exception e5)
    {
        Console->WriteLine("In Exception block");
        Console->WriteLine("Exception caught: %s", e5->ExceptionMessage);
        Console->WriteLine("Exception type: %s", e5->ExceptionType);
        e5->StackTrace();
    }
    /**
     * At the end of exception handling, the finally block resumes the execution leaving behind the error (Even if there's a Segmentation Fault). However, 
     * this block does not avoid the fact it could happen a new Segmenation Fault if the pointer which triggered the latest error is used again without 
     * checking it. 
     */
    finally
    {
        Console->WriteLine("");
        Console->WriteLine("4. Resume Execution");
        Console->WriteLine("First finally");
        /* Initialization of Array(String) */
        Console->WriteLine("");
        Console->WriteLine("5. Using Array collection");
        c = new Collection(String, 5);
        try
        {
            for (int i = 0; i < 5; i++)
            {
                /* Initialization of Int32 objects */
                d[i] = new Instance(Int32, 1);
                /* Constructor call */
                d[i]->Int32(i);
                /* Place into the array a new String element by calling the ToString() method which converts an Int32 to a String */
                c->SetValue(d[i]->ToString(), d[i]);
            }
            for (int i = 0; i < 5; i++)
            {
                /* The struct element is left-to-right, so this takes the object inside the Array and then gets the value to be printed out */
                Console->WriteLine(c->Get(i)->Get());
            }
            Console->WriteLine("");
            Console->WriteLine("6. Using standard Exception(). Unknown exception type");
            /* Throwing an exception to be handled in catch statement */
            throw new Exception();
        }
        /* Handler */
        catch(Exception e6)
        {
            /* Prints the ExceptionMessage, which is the generical one */
            Console->WriteLine(e6->ExceptionMessage);
        }
        /* Resume */
        finally
        {
            Console->WriteLine("");
            Console->WriteLine("7. Using Generic List collection");
            Console->WriteLine("Second finally");
            try
            {
                /* List initialization */
                e = new Generic(List(String));
                for(int i=0; i<5; i++)
                {
                    /* Append to the tail of the List */
                    e->Append(d[i]->ToString());
                }
                Console->WriteLine("Listing elements");
                /* Showing the elements */
                e->Show();
            }
            catch(Exception e7)
            {
                Console->WriteLine(e7->ExceptionMessage);
            }
            finally
            {
                Console->WriteLine("");
                Console->WriteLine("8. End of Execution");
                Console->WriteLine("Third finally and successful thread-execution");
            }
        }
    } 
    return 0;
}

/* Constructor */
optimize(0) void *A_A0(A * const this)
{
    /**
     * The destructor macro sets up the trampoline for the the method 
     * and then registers it into the Garbage Collector to be called 
     * right after goes out of scope (In case of main, wouldn't be shown 'cause it's called inside
     * fini section). 
     * Takes the class-like name, the method name and the arguments expected (Which has to be zero). 
     */
    destructor(A, $A, 0);
    /* This has to be initialized by default inside the compiler [WIP] */
    this->__vptrCount = 0;
    this->__vptrFlag = 0;
    /**
     * The virtual macro marks the memory address inside the Virtual Table structure for the specific
     * class-like and also sets the trampoline for the methods which can be overriden later in derived classes 
     * for polymorphic behavior.
     */
    virtual(A, virtualFunc, 1);
    virtual(A, virtualFunc2, 0);
    return 0;
}

/* Virtual Method with one argument */
void A_virtualFunc(int a, A * const this)
{
    Console->WriteLine("At %s with value: %d", __func__, a);
    /* Exception handling */
    try
    {
        FunctionThrow();
    }
    catch(Exception e)
    {
        Console->WriteLine("Exception caught: %s", e->ExceptionMessage);
        Console->WriteLine("Exception type: %s", e->ExceptionType);
        /* Stack Trace which starts from A_virtualFunc upwards */
        e->StackTrace();
    }
    finally
    {
        Console->WriteLine("");
    }
    return;
}

/* Virtual Method wihout arguments */
void A_virtualFunc2(A * const this)
{
    Console->WriteLine("At %s", __func__);
    return;
}

/* Destructor method which only displays a message */
void A_$A(A * const this)
{
    Console->WriteLine("Destroying A");
    return;
}

/**
 * Derived class from A. 
 * 
 * In this particular case (Class inheritance), the construct has several differences. Starting from the "base" macro. 
 * The base(T) macro calls the default constructor from the superclass and constructs the object using the portion of the superclass 
 * and then the constructor for its derived class performs the rest (Method overriding in this case).
 */
void *B_B0(B *this)
{
    Console->WriteLine("Constructing B");
    /* Calls the base constructor */
    base(A);
    /* This needs to be simplified [WIP] */
    this->vmtB = this->vmtA; 
    /* Placing the destructor for the subclass */
    destructor(B, $B, 0);
    /**
     * Method overriding for the superclass virtual methods. 
     * In here, by using the method names of the A's class, the subclass redefines it 
     * and the pointer of the Virtual Table goes to a different method which has the same signature 
     * of the base method but the class name of the method is slightly different (Should be the derived class name).
     */
    override(B, virtualFunc, 1); 
    override(B, virtualFunc2, 0);
    /* Calls the original method of the base class. This only works inside the constructor 'cause the base is scopped here only */
    base->virtualFunc(10); 
    return 0;
}

/* Method overriden with argument */
void B_virtualFunc(int a, B * const this)
{
    Console->WriteLine("At %s with value: %d", __func__, a);
}

/* Method overriden without arguments */
void B_virtualFunc2(B * const this)
{
    Console->WriteLine("At %s", __func__);
}

/* Destructor for the derived class */
void B_$B(B * const this)
{
    Console->WriteLine("Destroying B");
}

void FunctionThrow(void)
{
    /* Local pointer. This is set in Stack Segment and when an Exception occurs, the destructor will be rightfully called at Stack Unwinding */
    A *a = new stackalloc(A, 0);

    Console->WriteLine("At %s", __func__);
    /* Constructor call */
    a->A();
    Console->WriteLine("The following local variable will be destroyed by calling Local Destructors at Stack Unwinding process");
    /* Throwing a Segmentation Fault */
    throw new MemoryAccessException();
    return;
}