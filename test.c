#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int *a = malloc(4);
    void *b;
    char *c;

    *a = 10;
    b = a;
    c = b;
    printf("%c", *c);
}